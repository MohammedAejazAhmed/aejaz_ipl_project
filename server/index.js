const {averageWins} = require('./IPL');
const {extrasConceeded} = require('./IPL');
const {top10EconomicalBowlers} = require('./IPL');
const {totalMatchesPlayed} = require('./IPL');

averageWins();
extrasConceeded();
top10EconomicalBowlers();
totalMatchesPlayed();


