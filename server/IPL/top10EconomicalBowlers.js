const CSVToJSON = require('csvtojson');
const fs = require('fs');

function top10EconomicalBowlers (){
    let startingMatch = 0;
    let endingMatch = 0;

    //Calculating match id range of matches played in 2015

    CSVToJSON().fromFile('./../data/matches.csv').then(arrayOfMatchData=>{
    
        endingMatch = arrayOfMatchData.reduce((accumulator, element) => {
    
            if (parseInt(element['season']) == '2015' && startingMatch == 0) {
                
                startingMatch = parseInt(element['id']);
                accumulator = parseInt(element['id']);
            
            } else if (parseInt(element['season']) == '2015') {
                accumulator++;
            }

            return accumulator;
        });
    });
    
    CSVToJSON().fromFile('./../data/deliveries.csv').then(arrayOfDeliveryData=>{
    
        let bowler = 'none';
        let ballsByBowler = {};
        let runsByBowler = {};
    
        //calculating total runs conceeded and balls bowled by each bowler    
    
        arrayOfDeliveryData.reduce((accumulator, element) => {
    
            let currentBowler = element['bowler'];
    
            if (parseInt(element['match_id']) >= startingMatch && parseInt(element['match_id']) <= endingMatch) {
                
                if (currentBowler == bowler) {

                    runsByBowler[currentBowler] += parseInt(element['total_runs']);
                    ballsByBowler[currentBowler] += 1;
                
                } else if (currentBowler !== bowler) {
                    
                    if (runsByBowler.hasOwnProperty(currentBowler)) {
                        
                        runsByBowler[currentBowler] += parseInt(element['total_runs']);
                        ballsByBowler[currentBowler] += 1; 
                    
                    } else if (runsByBowler.hasOwnProperty(element['bowler']) == false) {
                        
                        runsByBowler[currentBowler] = parseInt(element['total_runs']);
                        ballsByBowler[currentBowler] = 1;
                    }

                    bowler = element['bowler'];
                }
            }
        }, 0);
    
        //calculating economies of bowlers
    
        const arrayBallsByBowler = Object.values(ballsByBowler);
        const arrayRunsByBowler = Object.values(runsByBowler);
        const bowlerNames = Object.keys(runsByBowler);
    
        const economyByBowler = arrayRunsByBowler.map((element, index) => {
    
            const economy = element / arrayBallsByBowler[index] * 6;
            object = {};
            object[bowlerNames[index]] = economy;
            return object;
        });
        
        //sorting economies of bowlers
    
        const arrayEconomyByBowler = economyByBowler.map((element, index) => {
            
            const key = Object.keys(element);
            const value = Object.values(element);
            return [key, value];
        });
    
        arrayEconomyByBowler.sort((a, b) => {
            return a[1] - b[1];
        });
        
        //dumping solution into json files in the output        
        
        const sortedEconomy = {};
    
        arrayEconomyByBowler.forEach((element, index)=>{
            if(index < 10){
                sortedEconomy[element[0][0]]=element[1][0];    
            }
        });        
        
        console.log(sortedEconomy);
        const jsonData = JSON.stringify(sortedEconomy);
        fs.writeFile('./../public/output/top10EconomicalBowlers.json',jsonData,(err)=>{
            if(err){
                throw err;
            }
            console.log("JSON data is saved");
        });
    });
}

module.exports = {
    top10EconomicalBowlers
}
