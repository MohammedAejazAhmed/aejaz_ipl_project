const CSVToJSON = require('csvtojson');
const fs = require('fs');

const averageWins = function(){

    //Parse csv data into array

    CSVToJSON().fromFile('./../data/matches.csv').then(arrayOfMatchData=>{
        
        const winsPerTeamPerYear = {};        
        
        // Calculating wins for each team for every year 

        arrayOfMatchData.reduce((accumulator, element)=>{

            let year = element['season']; 

            if(winsPerTeamPerYear.hasOwnProperty(element['winner'])){
                
                if(winsPerTeamPerYear[element['winner']].hasOwnProperty(element['season'])){
                    
                    winsPerTeamPerYear[element['winner']][element['season']] +=1;
                
                } else {

                    winsPerTeamPerYear[element['winner']][element['season']] = 1;
                }
            } else {

                winsPerTeamPerYear[element['winner']] = {};
                winsPerTeamPerYear[element['winner']][element['season']] = 1; 
            }
        });
        
        console.log(winsPerTeamPerYear);
        const jsonData = JSON.stringify(winsPerTeamPerYear);
        
        fs.writeFile('./../public/output/averageWins.json',jsonData,(err)=>{
            if(err){
                throw err;
            }
            console.log("JSON data is saved");
        });
        
    });
}

module.exports = {
    averageWins
}
