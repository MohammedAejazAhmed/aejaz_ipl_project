const CSVToJSON = require('csvtojson');
const fs = require('fs');

const totalMatchesPlayed = function(){ 

    CSVToJSON().fromFile('./../data/matches.csv').then(arrayOfMatchData=>{ 
        
        let year = '2017';
        const matchesPlayedPerYear = {};

        let last = parseInt(arrayOfMatchData.reduce((accumulator, element)=>{

            let currYear = element['season'];
            
            if(currYear === year){

                accumulator++;    
            
            } else {
           
                matchesPlayedPerYear[year] = parseInt(accumulator+1);
                year = currYear;
                accumulator = 0;
            } 

            return accumulator;
        }, 0));
        matchesPlayedPerYear[year] = last;
        
        console.log(matchesPlayedPerYear);
        
        //dumping solution to json files

        const data = JSON.stringify(matchesPlayedPerYear);
        
        fs.writeFile('./../public/output/totalMatchesPlayed.json',data,(err)=>{
            if(err){
                throw err;
            }
            console.log("JSON data is saved");
        });
    });
    
}

module.exports = {
    totalMatchesPlayed
}
