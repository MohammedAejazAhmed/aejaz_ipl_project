const CSVToJSON = require('csvtojson');
const fs = require('fs');

const extrasConceeded = function (){

    let startingMatch = 0;
    let endingMatch = 0;

    //Calculating match id range of matches played in 2016 
    
    CSVToJSON().fromFile('./../data/matches.csv').then(arrayOfMatchData=>{
    
        endingMatch = arrayOfMatchData.reduce((accumulator, element) => {
    
            if (parseInt(element['season']) == '2016' && startingMatch == 0) {
    
                startingMatch = parseInt(element['id']);
                accumulator = parseInt(element['id']);

            } else if (parseInt(element['season']) == '2016') {
                accumulator++;
            }
            return accumulator;
        });
    });

    //Calculating extras conceeded by each team played 

    CSVToJSON().fromFile('./../data/deliveries.csv').then(arrayOfDeliveryData=>{
    
        let extrasOfTeams = {};
        let bowlingTeam = 'none';
   
        arrayOfDeliveryData.reduce((accumulator, element) => {
    
            let currentBowlingTeam = element['bowling_team'];
    
            if (parseInt(element['match_id']) >= startingMatch &&       parseInt(element['match_id']) <= endingMatch) {
    
                if (currentBowlingTeam == bowlingTeam) {

                    extrasOfTeams[bowlingTeam] += parseInt(element['extra_runs']);

                } else if (currentBowlingTeam !== bowlingTeam) {
    

                    if (extrasOfTeams.hasOwnProperty(currentBowlingTeam)) {
    
                        extrasOfTeams[currentBowlingTeam] += parseInt(element['extra_runs']);

                    } else if (extrasOfTeams.hasOwnProperty(currentBowlingTeam) == false) {
    
                        extrasOfTeams[currentBowlingTeam] = parseInt(element['extra_runs']);
                    }
                }
            }
        }, 0);

        console.log(extrasOfTeams);
        
        //dumping the answer to json files in output

        const jsonData = JSON.stringify(extrasOfTeams);
        fs.writeFile('./../public/output/extrasConceeded.json',jsonData,(err)=>{
            if(err){
                throw err;
            }
            console.log("JSON data is saved");
        });

    });
}

module.exports = {
    extrasConceeded
}

