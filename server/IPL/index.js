module.exports = {

averageWins: require('./averageWins.js').averageWins,

extrasConceeded: require('./extrasConceeded.js').extrasConceeded,

top10EconomicalBowlers: require('./top10EconomicalBowlers.js').top10EconomicalBowlers,

totalMatchesPlayed: require('./totalMatchesPlayed.js').totalMatchesPlayed

}

