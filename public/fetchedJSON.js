//fetch extrasConceed.json and render data using highcharts

fetch('extrasConceeded.json')
   .then((res) => {
      return res.json();
   })
   .then((res) => {
      const teamArray = Object.keys(res);
      const extrasConceeded = Object.values(res);
      console.log(teamArray);
      $(document).ready(function () {
         const chart = {
            type: 'column'
         };
         const title = {
            text: 'Extras conceeded by each team'
         };
         const subtitle = {
            text: 'Source: kaggle.com'
         };
         const xAxis = {
            categories: teamArray,
            crosshair: true
         };
         const yAxis = {
            min: 0,
            title: {
               text: 'extrasConceeded'
            }
         };
         const tooltip = {
            headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style = "color:{series.color};padding:0">{series.name}: </td>' +
               '<td style = "padding:0"><b>{point.y} runs</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
         };
         const plotOptions = {
            column: {
               pointPadding: 0.2,
               borderWidth: 0
            }
         };
         const credits = {
            enabled: false
         };
         const series = [{
            name: "Extras Conceeded",
            data: extrasConceeded
         }];

         const json = {};
         json.chart = chart;
         json.title = title;
         json.subtitle = subtitle;
         json.tooltip = tooltip;
         json.xAxis = xAxis;
         json.yAxis = yAxis;
         json.series = series;
         json.plotOptions = plotOptions;
         json.credits = credits;
         $('#container2').highcharts(json);

      });
   });

//fetch totalMatchesPlayed.json and render data using highcharts

fetch('totalMatchesPlayed.json')
   .then((res) => {
      return res.json();
   })
   .then((res) => {

      const yearArray = Object.keys(res);
      const matchesPlayedArray = Object.values(res);
      console.log(yearArray);
      $(document).ready(function () {
         const chart = {
            type: 'column'
         };
         const title = {
            text: 'Number of matches played every year'
         };
         const subtitle = {
            text: 'Source: kaggle.com'
         };
         const xAxis = {
            categories: yearArray,
            title: {
               text: 'Year'
            },
            crosshair: true
         };
         const yAxis = {
            min: 0,
            title: {
               text: 'Matches played'
            }
         };
         const tooltip = {
            headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style = "color:{series.color};padding:0">{series.name}: </td>' +
               '<td style = "padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
         };
         const plotOptions = {
            column: {
               pointPadding: 0.2,
               borderWidth: 0
            }
         };
         const credits = {
            enabled: false
         };
         const series = [{
            name: "Matches Played",
            data: matchesPlayedArray
         }];

         const json = {};
         json.chart = chart;
         json.title = title;
         json.subtitle = subtitle;
         json.tooltip = tooltip;
         json.xAxis = xAxis;
         json.yAxis = yAxis;
         json.series = series;
         json.plotOptions = plotOptions;
         json.credits = credits;
         $('#container1').highcharts(json);

      });
   });

//fetch averageWins.json and render data using highcharts

fetch('averageWins.json')
   .then((res) => {
      return res.json();
   })
   .then((res) => {

      const teamArray = Object.keys(res);
      const yearByWins = Object.values(res);

      const valueByYear = new Array(teamArray.length);
      for (let iterate = 0; iterate < valueByYear.length; iterate++) {
         valueByYear[iterate] = new Array(10);
      }
      for (let iterate = 0; iterate < teamArray.length; iterate++) {
         for (let iterate1 = 0; iterate1 < 10; iterate1++) {
            valueByYear[iterate][iterate1] = 0;
         }
      }

      for (let iterate1 = 0; iterate1 < valueByYear.length; iterate1++) {
         for (let iterate2 = 0; iterate2 < valueByYear[iterate1].length; iterate2++) {

            let year = 2008 + iterate2;

            if (yearByWins[iterate1].hasOwnProperty(year.toString())) {

               valueByYear[iterate1][iterate2] = parseInt(yearByWins[iterate1][year.toString()]);

            }
         }
      }

      console.log(valueByYear);
      $(document).ready(function () {
         const chart = {
            type: 'column'
         };
         const title = {
            text: 'Total wins per team per year'
         };
         const subtitle = {
            text: 'kaggle.com'
         };
         const xAxis = {
            categories: ["2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017"],
            title: {
               text: "Years"
            },
            crosshair: true
         };
         const yAxis = {
            min: 0,
            title: {
               text: 'wins per year'
            }
         };
         const tooltip = {
            headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style = "color:{series.color};padding:0">{series.name}: </td>' +
               '<td style = "padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
         };
         const plotOptions = {
            column: {
               pointPadding: 0.2,
               borderWidth: 0
            }
         };
         const credits = {
            enabled: false
         };

         const series = [];
         for (let iterate = 0; iterate < teamArray.length; iterate++) {
            const obj = {
               name: teamArray[iterate],
               data: valueByYear[iterate]
            };
            series.push(obj);
         }

         const json = {};
         json.chart = chart;
         json.title = title;
         json.subtitle = subtitle;
         json.tooltip = tooltip;
         json.xAxis = xAxis;
         json.yAxis = yAxis;
         json.series = series;
         json.plotOptions = plotOptions;
         json.credits = credits;
         $('#container3').highcharts(json);

      });
   });

//fetch top10EconomicalBowlers.json and render data using highcharts

fetch('top10EconomicalBowlers.json')
   .then((res) => {
      return res.json();
   })
   .then((res) => {

      const Name = Object.keys(res);
      const Economy = Object.values(res);

      $(document).ready(function () {
         const title = {
            text: 'Top 10 most economical bowlers'
         };
         const subtitle = {
            text: 'Source: kaggle.com'
         };
         const xAxis = {
            categories: Name,
            title: {
               text: 'Bowler Name'
            }
         };
         const yAxis = {
            title: {
               text: 'Economy'
            },
            plotLines: [{
               value: 0,
               width: 1,
               color: '#808080'
            }]
         };
         const tooltip = {
            valueSuffix: null
         }
         const legend = {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
         };
         const series = [{
            name: "Economy",
            data: Economy
         }];

         const json = {};
         json.title = title;
         json.subtitle = subtitle;
         json.xAxis = xAxis;
         json.yAxis = yAxis;
         json.tooltip = tooltip;
         json.legend = legend;
         json.series = series;

         $('#container4').highcharts(json);
      });

   });